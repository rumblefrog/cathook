#!/usr/bin/env bash

# $1 TUI 
# $2 INIT
# $3 Pre update

CURR_VERSION=6

TUI=${1:-false}
INIT=${2:-false}
PRE_UPDATE=${3:-false}

set -e

configfile="./scripts/updater-preferences"
. ./scripts/config.shlib

tui=true
if [ ! -x "$(command -v dialog)" ]; then
    tui=false
    echo -e "\033[1;31mTerminal UI NOT supported! Install \"dialog\"! \033[0m"
fi
if [ ! -t 0 ]; then
    tui=false
fi

#
# Git check, migrate users from .zip to git repos
#
if [ "$PRE_UPDATE" == "true" ]; then
if [ ! -x "$(command -v git)" ]; then
    # install only git
    echo git not installed!
    ./scripts/dependencycheck true
fi
if [ ! -d "./.git" ]; then
    git init
    git remote add origin https://gitlab.com/nullworks/cathook/cathook
fi
fi

#
# Init goes here
#
function init() {
    if [ "$INIT" == true ]; then
        cfg_write $configfile update_channel stable
        cfg_write $configfile version $CURR_VERSION
    fi
}

#
# Migrations go here
#

function updateRepoURL() {
    local URL="$(curl --max-time 10 -Ls -o /dev/null -w %{url_effective} https://cathook.club/s/ch/git || echo error)"
    local GIT_REMOTE=$(git config --get remote.origin.url || echo unknown)
    if [ "$URL" != "error" ] && [ "$GIT_REMOTE" != "$URL" ]; then
        git remote set-url origin "$URL"
        echo -e "\033[1;33m\n\nMigrations: Updated remote URL to new repo! Welcome to $URL!\n\n\033[0m"
    fi
}

function migrations() {
    exists=true
    cfg_haskey $configfile version || exists=false
    if [ "$exists" == true ]; then
        version="$(cfg_read $configfile version)"
    else
        # No version string. Nothing we can do.
        return 0
    fi
    
    if (( $version > $CURR_VERSION )); then
        cfg_write $configfile version $CURR_VERSION
    fi

    # Hack to fix compile error in version 1 to 4 caused by CoTiRe on legacy ubuntu.
    if [ "$version" -lt 5 ]; then
        cfg_write $configfile version 5
        if [ -d "./build/CMakeFiles/cathook.dir" ]; then
            rm -r ./build/CMakeFiles/cathook.dir
        fi
    fi
    if [ "$version" -lt 6 ]; then
        cfg_write $configfile version 6
        GIT_REMOTE=$(git config --get remote.origin.url || echo unknown)
        if [ "$GIT_REMOTE" == "https://github.com/nullworks/cathook.git" ] || [ "$GIT_REMOTE" == "https://github.com/nullworks/cathook" ]; then
            updateRepoURL
        else
            echo -e '\033[1;33m\n\nMigrations: Did not update the remote URL because you are not using the default repo URLs set by our install scripts! We are now at https://gitlab.com/nullworks/cathook/cathook!\n\n\033[0m'
        fi
    fi

    if [ "$PRE_UPDATE" == "true" ]; then
        updateRepoURL
    fi
}

if [ "$PRE_UPDATE" == "true" ] && [ "$INIT" == "true" ]; then
    # Our job is done here. We dont want to set a config version here yet.
    exit
fi

if [ "$INIT" == true ]; then
    init
else
    migrations
fi
